package com.practice.backend.mapper;

import com.example.springbackend.entity.Post;
import org.mapstruct.Mapper;
import com.example.springbackend.model.PostRespDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PostMapper {

    List<PostRespDto> toDtos(List<Post> entities);

    PostRespDto toDto(Post entity);

}