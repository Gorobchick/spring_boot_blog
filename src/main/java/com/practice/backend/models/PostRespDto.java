package com.practice.backend.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import jakarta.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

@JsonTypeName("PostResp")
public class PostRespDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("anons")
    private String anons;

    @JsonProperty("fullText")
    private String fullText;

    public PostRespDto id(String id) {
        this.id = id;
        return this;
    }

    @Size(max = 100)
    @Schema(name = "id", example = "da23b2ac-a487-11ed-a8fc-0242ac120002", required = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PostRespDto title(String title) {
        this.title = title;
        return this;
    }

    @Size(max = 100)
    @Schema(name = "title", example = "Title", required = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PostRespDto anons(String anons) {
        this.anons = anons;
        return this;
    }

    @Size(max = 100)
    @Schema(name = "anons", example = "", required = false)
    public String getAnons() {
        return anons;
    }

    public void setAnons(String anons) {
        this.anons = anons;
    }

    public PostRespDto fullText(String fullText) {
        this.fullText = fullText;
        return this;
    }

    @Size(max = 1000)
    @Schema(name = "fullText", example = "", required = false)
    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PostRespDto companyResp = (PostRespDto) o;
        return Objects.equals(this.id, companyResp.id) &&
                Objects.equals(this.title, companyResp.title) &&
                Objects.equals(this.anons, companyResp.anons) &&
                Objects.equals(this.fullText, companyResp.fullText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, anons, fullText);
    }

    @Override
    public String toString() {
        return "class CompanyRespDto {\n" +
                "    id: " + toIndentedString(id) + "\n" +
                "    title: " + toIndentedString(title) + "\n" +
                "    anons: " + toIndentedString(anons) + "\n" +
                "    fullText: " + toIndentedString(fullText) + "\n" +
                "}";
    }

    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
