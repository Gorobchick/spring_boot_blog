package com.practice.backend.controllers;

import com.example.springbackend.service.BlogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/Blohg")
public class BlogController {

    final
    BlogService BlogService;

    public BlogController(BlogService BlogService) {
        this.BlogService = BlogService;
    }

    @GetMapping(value = "/producer")
    public String producer(@RequestParam("message") String message) {
        BlogService.send(message);

        return "Message sent to the example_topic Successfully";
    }

}
