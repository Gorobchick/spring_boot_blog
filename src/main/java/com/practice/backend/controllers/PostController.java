package com.practice.backend.controllers;

import com.example.springbackend.entity.User;
import com.example.springbackend.repositoPost;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PostController {

    private final PostRepository postRepository;

    public PostControllPost postRepository) {
        this.postRepository = postRepository;
    }

    @RequestMapping("/posts")
    public List<Post> listAll() {
        return postRepository.findAll();
    }

}
